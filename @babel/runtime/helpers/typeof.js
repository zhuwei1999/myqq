var o = require("../../../@babel/runtime/helpers/typeof");

function t(e) {
    return "function" == typeof Symbol && "symbol" == o(Symbol.iterator) ? module.exports = t = function(t) {
        return o(t);
    } : module.exports = t = function(t) {
        return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : o(t);
    }, t(e);
}

module.exports = t;