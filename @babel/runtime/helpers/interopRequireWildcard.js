var e = require("./typeof");

function t() {
    if ("function" != typeof WeakMap) return null;
    var e = new WeakMap();
    return t = function() {
        return e;
    }, e;
}

module.exports = function(r) {
    if (r && r.__esModule) return r;
    if (null === r || "object" !== e(r) && "function" != typeof r) return {
        default: r
    };
    var n = t();
    if (n && n.has(r)) return n.get(r);
    var o = {}, u = Object.defineProperty && Object.getOwnPropertyDescriptor;
    for (var f in r) if (Object.prototype.hasOwnProperty.call(r, f)) {
        var i = u ? Object.getOwnPropertyDescriptor(r, f) : null;
        i && (i.get || i.set) ? Object.defineProperty(o, f, i) : o[f] = r[f];
    }
    return o.default = r, n && n.set(r, o), o;
};