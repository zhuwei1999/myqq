function r(r, n) {
    r[n >> 5] |= 128 << n % 32, r[14 + (n + 64 >>> 9 << 4)] = n;
    for (var f = 1732584193, a = -271733879, i = -1732584194, h = 271733878, l = 0; l < r.length; l += 16) {
        var v = f, g = a, A = i, d = h;
        f = t(f, a, i, h, r[l + 0], 7, -680876936), h = t(h, f, a, i, r[l + 1], 12, -389564586), 
        i = t(i, h, f, a, r[l + 2], 17, 606105819), a = t(a, i, h, f, r[l + 3], 22, -1044525330), 
        f = t(f, a, i, h, r[l + 4], 7, -176418897), h = t(h, f, a, i, r[l + 5], 12, 1200080426), 
        i = t(i, h, f, a, r[l + 6], 17, -1473231341), a = t(a, i, h, f, r[l + 7], 22, -45705983), 
        f = t(f, a, i, h, r[l + 8], 7, 1770035416), h = t(h, f, a, i, r[l + 9], 12, -1958414417), 
        i = t(i, h, f, a, r[l + 10], 17, -42063), a = t(a, i, h, f, r[l + 11], 22, -1990404162), 
        f = t(f, a, i, h, r[l + 12], 7, 1804603682), h = t(h, f, a, i, r[l + 13], 12, -40341101), 
        i = t(i, h, f, a, r[l + 14], 17, -1502002290), f = u(f, a = t(a, i, h, f, r[l + 15], 22, 1236535329), i, h, r[l + 1], 5, -165796510), 
        h = u(h, f, a, i, r[l + 6], 9, -1069501632), i = u(i, h, f, a, r[l + 11], 14, 643717713), 
        a = u(a, i, h, f, r[l + 0], 20, -373897302), f = u(f, a, i, h, r[l + 5], 5, -701558691), 
        h = u(h, f, a, i, r[l + 10], 9, 38016083), i = u(i, h, f, a, r[l + 15], 14, -660478335), 
        a = u(a, i, h, f, r[l + 4], 20, -405537848), f = u(f, a, i, h, r[l + 9], 5, 568446438), 
        h = u(h, f, a, i, r[l + 14], 9, -1019803690), i = u(i, h, f, a, r[l + 3], 14, -187363961), 
        a = u(a, i, h, f, r[l + 8], 20, 1163531501), f = u(f, a, i, h, r[l + 13], 5, -1444681467), 
        h = u(h, f, a, i, r[l + 2], 9, -51403784), i = u(i, h, f, a, r[l + 7], 14, 1735328473), 
        f = e(f, a = u(a, i, h, f, r[l + 12], 20, -1926607734), i, h, r[l + 5], 4, -378558), 
        h = e(h, f, a, i, r[l + 8], 11, -2022574463), i = e(i, h, f, a, r[l + 11], 16, 1839030562), 
        a = e(a, i, h, f, r[l + 14], 23, -35309556), f = e(f, a, i, h, r[l + 1], 4, -1530992060), 
        h = e(h, f, a, i, r[l + 4], 11, 1272893353), i = e(i, h, f, a, r[l + 7], 16, -155497632), 
        a = e(a, i, h, f, r[l + 10], 23, -1094730640), f = e(f, a, i, h, r[l + 13], 4, 681279174), 
        h = e(h, f, a, i, r[l + 0], 11, -358537222), i = e(i, h, f, a, r[l + 3], 16, -722521979), 
        a = e(a, i, h, f, r[l + 6], 23, 76029189), f = e(f, a, i, h, r[l + 9], 4, -640364487), 
        h = e(h, f, a, i, r[l + 12], 11, -421815835), i = e(i, h, f, a, r[l + 15], 16, 530742520), 
        f = o(f, a = e(a, i, h, f, r[l + 2], 23, -995338651), i, h, r[l + 0], 6, -198630844), 
        h = o(h, f, a, i, r[l + 7], 10, 1126891415), i = o(i, h, f, a, r[l + 14], 15, -1416354905), 
        a = o(a, i, h, f, r[l + 5], 21, -57434055), f = o(f, a, i, h, r[l + 12], 6, 1700485571), 
        h = o(h, f, a, i, r[l + 3], 10, -1894986606), i = o(i, h, f, a, r[l + 10], 15, -1051523), 
        a = o(a, i, h, f, r[l + 1], 21, -2054922799), f = o(f, a, i, h, r[l + 8], 6, 1873313359), 
        h = o(h, f, a, i, r[l + 15], 10, -30611744), i = o(i, h, f, a, r[l + 6], 15, -1560198380), 
        a = o(a, i, h, f, r[l + 13], 21, 1309151649), f = o(f, a, i, h, r[l + 4], 6, -145523070), 
        h = o(h, f, a, i, r[l + 11], 10, -1120210379), i = o(i, h, f, a, r[l + 2], 15, 718787259), 
        a = o(a, i, h, f, r[l + 9], 21, -343485551), f = c(f, v), a = c(a, g), i = c(i, A), 
        h = c(h, d);
    }
    return Array(f, a, i, h);
}

function n(r, n, t, u, e, o) {
    return c((f = c(c(n, r), c(u, o))) << (a = e) | f >>> 32 - a, t);
    var f, a;
}

function t(r, t, u, e, o, c, f) {
    return n(t & u | ~t & e, r, t, o, c, f);
}

function u(r, t, u, e, o, c, f) {
    return n(t & e | u & ~e, r, t, o, c, f);
}

function e(r, t, u, e, o, c, f) {
    return n(t ^ u ^ e, r, t, o, c, f);
}

function o(r, t, u, e, o, c, f) {
    return n(u ^ (t | ~e), r, t, o, c, f);
}

function c(r, n) {
    var t = (65535 & r) + (65535 & n);
    return (r >> 16) + (n >> 16) + (t >> 16) << 16 | 65535 & t;
}

function f(r) {
    for (var n = Array(), t = 0; t < 8 * r.length; t += 8) n[t >> 5] |= (255 & r.charCodeAt(t / 8)) << t % 32;
    return n;
}

module.exports = {
    hex_md5: function(n) {
        return function(r) {
            for (var n = "0123456789abcdef", t = "", u = 0; u < 4 * r.length; u++) t += n.charAt(r[u >> 2] >> u % 4 * 8 + 4 & 15) + n.charAt(r[u >> 2] >> u % 4 * 8 & 15);
            return t;
        }(r(f(n), 8 * n.length));
    },
    b64_md5: function(n) {
        return function(r) {
            for (var n = "", t = 0; t < 4 * r.length; t += 3) for (var u = (r[t >> 2] >> t % 4 * 8 & 255) << 16 | (r[t + 1 >> 2] >> (t + 1) % 4 * 8 & 255) << 8 | r[t + 2 >> 2] >> (t + 2) % 4 * 8 & 255, e = 0; e < 4; e++) 8 * t + 6 * e > 32 * r.length ? n += "" : n += "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/".charAt(u >> 6 * (3 - e) & 63);
            return n;
        }(r(f(n), 8 * n.length));
    }
};