var e = require("md5.js"), t = function(e) {
    return (e = e.toString())[1] ? e : "0" + e;
}, n = function(t) {
    var n = getApp().loginUserData ? getApp().loginUserData.userId : "", o = getApp().loginUserData ? getApp().loginUserData.token : "";
    return {
        token: o,
        userId: n,
        timestamp: Date.now(),
        sign: e.hex_md5(Date.now() + o + n),
        Authorization: "85c598e4-ef2f-11e9-9db6-a6a44159fbe0"
    };
}, o = function(e) {
    return null !== e && "" !== e.trim();
};

module.exports = {
    formatTime: function(e) {
        var n = e.getFullYear(), o = e.getMonth() + 1, a = e.getDate(), i = e.getHours(), r = e.getMinutes(), s = e.getSeconds();
        return [ n, o, a ].map(t).join("/") + " " + [ i, r, s ].map(t).join(":");
    },
    formatTimeYMD: function(e) {
        return [ e.getMonth() + 1, e.getDate() ].map(t).join("/");
    },
    requestHeader: n,
    doRequest: function(e) {
        var t = {
            title: e.loainngMsg ? e.loainngMsg : "连接中"
        };
        wx.showLoading(t), wx.request({
            url: getApp().hostAddress + e.url,
            data: e.data ? e.data : {},
            method: e.method ? e.method : "POST",
            header: n(),
            success: function(t) {
                if (t.data && -2 === t.data.errorCode) return wx.hideLoading(), void (getApp().loginModelFlag || (getApp().needLogin = !0, 
                getApp().loginModelFlag = !0, console.log("请求[" + e.url + "]时token过期"), wx.showModal({
                    title: "提示",
                    content: "账号信息已过期，请重新登录",
                    showCancel: !1,
                    success: function(e) {
                        getApp().loginModelFlag = !1, wx.navigateTo({
                            url: "../login/login",
                            success: function() {},
                            fail: function() {},
                            complete: function() {}
                        });
                    }
                })));
                e.success ? e.success(t) : wx.showModal({
                    title: "成功",
                    content: "数据提交成功",
                    showCancel: !1,
                    success: function(e) {}
                });
            },
            fail: function(t) {
                e.fail ? e.fail(t) : wx.showModal({
                    title: "错误",
                    content: "连接服务器失败，请稍后重试",
                    showCancel: !1,
                    success: function(t) {
                        e.failAfter && e.failAfter(t);
                    }
                });
            },
            complete: function(t) {
                e.complete ? e.complete(t) : wx.hideLoading();
            }
        });
    },
    isNotBlank: o,
    isBlank: function(e) {
        return !o(e);
    },
    formObjToArr: function(e) {
        var t = [];
        for (var n in e) t.push({
            value: e[n],
            name: n
        });
        return t;
    }
};