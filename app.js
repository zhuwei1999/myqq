App({
    hostAddress: "https://surname.yuanshudata.cn",
    userAnswerId: null,
    surenCheckpoint: 0,
    loginUserWXOpenId: null,
    loginUserSessionKey: null,
    defaultShareTitle: "",
    loginUserData: {
        loginName: null,
        token: null,
        userId: null,
        userName: null,
        userType: null
    },
    needRefreshList: !1,
    needLogin: !0,
    loginModelFlag: !1,
    operationBar: !1,
    onLaunch: function() {},
    checkForLogin: function(e) {
        getApp().loginUserWXOpenId || getApp().loginUserBlock ? e && e() : (getApp().loginUserBlock = !0, 
        wx.login({
            success: function(n) {
                n.code ? wx.request({
                    url: getApp().hostAddress + "/weChat/v1/user/session/" + n.code,
                    data: {},
                    method: "GET",
                    header: {},
                    success: function(n) {
                        console.log("wechat login"), console.log(n), n.data.session_key && n.data.openid ? (getApp().loginUserSessionKey = n.data.session_key, 
                        getApp().loginUserWXOpenId = n.data.openid, e && e()) : wx.showModal({
                            title: "提示",
                            content: "获取登录信息出错，请稍后重试",
                            showCancel: !1
                        });
                    },
                    fail: function() {
                        wx.showModal({
                            title: "提示",
                            content: "连接服务器失败，请稍后重试",
                            showCancel: !1
                        });
                    },
                    complete: function() {
                        getApp().loginUserBlock = !1;
                    }
                }) : console.log("登录失败！" + n.errMsg);
            },
            fail: function() {
                wx.showModal({
                    title: "提示",
                    content: "登录失败，请检查您的网络",
                    showCancel: !1
                });
            },
            complete: function() {
                getApp().loginUserBlock = !1;
            }
        }));
    },
    downloadImage: function(e, n) {
        wx.downloadFile({
            url: n.replace("http", "https"),
            success: function(n) {
                200 === n.statusCode && wx.getFileSystemManager().saveFile({
                    tempFilePath: n.tempFilePath,
                    success: function(n) {
                        wx.setStorageSync(e, n.savedFilePath), console.log(e + ": 缓存成功");
                    }
                });
            },
            fail: function(e) {
                console.log("缓存失败");
            }
        });
    }
});