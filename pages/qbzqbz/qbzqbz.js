require("../../@babel/runtime/helpers/interopRequireDefault"), require("../../@babel/runtime/helpers/interopRequireWildcard");

var e = require("../../@babel/runtime/helpers/defineProperty"), t = require("../../utils/util.js"), o = require("../../components/ad-custom-unit/videoAd/index").adVideoUtils;

Page({
    data: {
        showMoreOriginal: !1,
        contentHeight: 0,
        showMoreFamous: !1,
        famousPeopleHeight: 0,
        ec: {
            lazyLoad: !0
        },
        detailsInfo: {},
        echartsComponnet: null,
        GIFUrl: "",
        showGIF: !0
    },
    handleShowMoreOriginal: function() {
        wx.createSelectorQuery();
        var e = this.data.showMoreOriginal;
        this.setData({
            showMoreOriginal: !e
        });
    },
    handleNext: function() {
        var e = new Date(), t = Math.abs(e - getApp().scanTime) <= 3e5;
        console.log("isInFiveMinutes: ", t), "1" == getApp().local && t && this.handleSendToScreen();
    },
    handleShowMoreFamous: function() {
        var e = this, t = wx.createSelectorQuery(), o = e.data, a = o.famousPeopleHeight, n = o.showMoreFamous;
        t.select("#people").boundingClientRect(function(t) {
            t.height && (n = !n, a = Number(t.height) + 10, e.setData({
                showMoreFamous: n,
                famousPeopleHeight: a
            }));
        }).exec();
    },
    onLoad: function(e) {
        var t = this;
        this.handleGetDetailsInfo(e.value), o.videoAdLoad("adunitId", function() {
            t.handleDownloadSurnameImg();
        });
    },
    onReady: function() {},
    onShow: function() {},
    onHide: function() {},
    onUnload: function() {
        wx.pageScrollTo({
            duration: 0,
            scrollTop: 0
        });
    },
    onPullDownRefresh: function() {},
    onReachBottom: function() {},
    onShareAppMessage: function() {},
    handleGetDetailsInfo: function(e) {
        var o = this;
        e && (o.data.detailsInfo, t.doRequest({
            url: "/wechat/v1/surname/surnameSearch?surname=" + e,
            method: "GET",
            success: function(e) {
                if (e.data && 0 == e.data.errorCode) {
                    var t = e.data.result;
                    t.gifKey ? (t.gifKey = t.gifKey.replace("http", "https"), setTimeout(function() {
                        o.setData({
                            showGIF: !1
                        });
                    }, 5e3)) : o.setData({
                        showGIF: !1
                    }), o.setData({
                        detailsInfo: t
                    }), o.handleNext(), console.log(t);
                    var a = wx.createSelectorQuery(), n = o.data.contentHeight;
                    a.select("#content").boundingClientRect(function(e) {
                        e.height && (n = Number(e.height) + 10, o.setData({
                            contentHeight: n
                        }));
                    }).exec();
                }
            },
            fail: function(e) {},
            complete: function(e) {
                wx.hideLoading();
            }
        }));
    },
    init_echarts: function() {
        var e = this;
        e.echartsComponnet.init(function(t, o, i) {
            var s = a.init(t, null, {
                width: o,
                height: i
            });
            return a.registerMap("china", n.default), s.setOption(e.getOption()), s;
        });
    },
    getOption: function() {
        var t;
        console.log("地图数据: ", this.data.detailsInfo.provinceVOS);
        var o = this.data.detailsInfo.provinceVOS;
        return {
            tooltip: {
                trigger: "item",
                textStyle: {
                    color: "#5C371F"
                },
                formatter: function(e) {
                    console.log(e.value);
                    var t = e.name ? e.name : "获取中";
                    return e.value && e.value, "".concat(t, " ");
                }
            },
            visualMap: {
                type: "continuous",
                left: "left",
                top: "bottom",
                show: !1
            },
            series: [ {
                type: "map",
                mapType: "china",
                label: {
                    normal: {
                        show: !1
                    },
                    emphasis: {
                        textStyle: {
                            color: "#fff"
                        }
                    }
                },
                select: {
                    label: {
                        show: !1
                    }
                },
                itemStyle: {
                    normal: {
                        borderColor: "#5C371F",
                        areaColor: "#e2d1c3"
                    },
                    emphasis: (t = {
                        areaColor: "#5C371F",
                        borderWidth: 0,
                        shadowOffsetX: 0,
                        shadowOffsetY: 0,
                        shadowBlur: 20
                    }, e(t, "borderWidth", 0), e(t, "shadowColor", "rgba(0, 0, 0, 0.5)"), t)
                },
                data: o
            } ]
        };
    },
    handleGetImageUrl: function(e) {
        var o = this;
        t.doRequest({
            url: "/static/v1/common/ossFile/getUrl?code=" + e,
            method: "GET",
            success: function(e) {
                e.data.result && 0 == e.data.errorCode && (getApp().downloadImage("GIFUrl", e.data.result[0]), 
                o.setData({
                    GIFUrl: e.data.result[0],
                    showGIF: !0
                }));
            },
            fail: function(e) {},
            complete: function(e) {
                wx.hideLoading();
            }
        });
    },
    handleFontImage: function() {
        var e = this;
        o.showModel("观看一次广告，即可免费下载字体图.", function() {
            e.handleDownloadSurnameImg();
        });
       
        
        
    },
    handleDownloadSurnameImg: function() {
        var e = this.data.detailsInfo.surnameTotem;
        wx.downloadFile({
            url: e.replace("http", "https"),
            success: function(e) {
                200 === e.statusCode && wx.saveImageToPhotosAlbum({
                    filePath: e.tempFilePath,
                    success: function(e) {
                        wx.showToast({
                            title: "图片保存成功",
                            duration: 2e3
                        });
                    }
                });
            },
            fail: function(e) {
                console.log("缓存失败");
            }
        });
    },
    handleSendToScreen: function() {
        t.doRequest({
            url: "/wechat/v1/surname/sendToScreenV2?macAddress=" + this.data.detailsInfo.macAddress,
            method: "POST",
            success: function(e) {
                console.log(e);
            },
            fail: function(e) {}
        });
    }
});