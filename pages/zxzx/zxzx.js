var e = require("../../utils/util.js");


Page({
    data: {
        familyNameImageUrl: "",
        showGIF: !0,
        surnameValue: "",
        isInScene: !1,
        userInfo: null,
        enumValue: "",
        enumValueList: [],
        showSystemRadio: !1,
        manageLogin: !1
    },
    methods: {
        gotoMarket() {
            // #ifdef APP-PLUS
            if (uni.getSystemInfoSync().platform == "ios") {
                // 这里填写appstore应用id
                let appstoreid = Globalunit.iosMarketId; // 'id1417078253';
                plus.runtime.openURL("itms-apps://" + 'itunes.apple.com/cn/app/wechat/' + appstoreid + '?mt=8');
            }
            if (uni.getSystemInfoSync().platform == "android") {
                var Uri = plus.android.importClass("android.net.Uri");
                // var uri = Uri.parse("market://details?id=" + Globalunit.androidMarketId);
                var uri = Uri.parse(Globalunit.androidMarketUrl);
                var Intent = plus.android.importClass('android.content.Intent');
                var intent = new Intent(Intent.ACTION_VIEW, uri);
                var main = plus.android.runtimeMainActivity();
                main.startActivity(intent);
            }
            // #endif
        },
        onclose(e) {
            console.log("onclose: " + e.detail);
        },
        onerror(e) {
            console.log("onerror: " + e.detail.errCode + " message:: " + e.detail.errMsg);
        },
        async checkVersion() {
            let res = await callCheckVersion()
            console.log("res: ", res);
            if (res.result.code > 0) {
                updateVersion()
            } else {
                uni.showToast({
                    title: '当前已经是最新版本！',
                    icon: 'none'
                });
            }
        },
        goxiaoxi() {
            uni.navigateTo({
                url: '/pages/xiaoxi/xiaoxi',
            });
        },
        goset() {
            uni.navigateTo({
                url: '/pages/set/set?userinfo=' + encodeURIComponent(JSON.stringify(this.user)),
            });
        },
        toFeedback() {
            uni.navigateTo({
                url: '/uni_modules/uni-feedback/pages/opendb-feedback/list',
                complete(e) {
                    console.log("e: ", e);
                }
            });
        },
        goqudao() {
            uni.navigateTo({
                url: '/pages/qudao/qudao',
            });
        },
        goqianbao() {
            uni.navigateTo({
                url: '/pages/qianbao/qianbao?num=' + this.user.fen,
            });
        },
        goyaoqingma() {
            uni.navigateTo({
                url: '/pages/yaoqingma/yaoqingma',
            });
        },
        gobangzhu() {
            uni.navigateTo({
                url: '/pages/bangzhu/bangzhu',
            });
        },
        goshiming() {
            uni.navigateTo({
                url: '/pages/shiming/shiming',
            });
            // if(this.user.isauthentication == 0){
            // 	uni.navigateTo({
            // 		url: '/pages/shiming/shiming‘
            // 	});
            // }else{
            // 	uni.showToast({
            // 		title: "已完成实名认证!",
            // 		icon: 'none',
            // 	});
            // }
        },
        gofenhongbao() {
            uni.navigateTo({
                url: '/pages/fenhongmao/fenhongmao?jindu=' + this.jindu,
            });
        },
        getuserinfo() {
            var self = this
            uni.showLoading()
            uniCloud.callFunction({
                name: 'hallctrl',
                data: {
                    action: "hall/getuserinfo",
                    data: {
                        uid: self.uid ? self.uid : uni.getStorageSync('uid'),
                        uniIdToken: self.token ? self.token : uni.getStorageSync('uni_id_token')
                    }
                }
            }).then((res) => {
                uni.hideLoading()
                console.log("getuserinfo-------", res)
                if (res.result.code && res.result.code != 0) {
                    uni.showToast({
                        title: res.result.message,
                        icon: 'none',
                    });
                    return
                }
                if (res.result.errcode && res.result.errcode != 0) {
                    uni.showToast({
                        title: res.result.errmsg,
                        icon: 'none',
                    });
                    if (res.result.errcode == 10005) {
                        uni.removeStorage({
                            key: 'uni_id_token',
                            success: function (res) {

                            }
                        });
                        uni.removeStorage({
                            key: 'uid',
                            success: function (res) {
                                uni.navigateTo({
                                    url: '/pages/login/login',
                                });
                            }
                        });
                    }
                    return
                }
                self.user = res.result.data.user
                var bide = Number(res.result.data.user.bide)
                if (bide > 100) {
                    bide = 100
                }
                self.jindu = bide
            }).catch((err) => {
                uni.hideLoading()
                // uni.showToast({
                // 	title: "获取用户信息失败，请稍后重试!",
                // 	icon: 'none',
                // });
            })
        }
    },
    methods: {
        urlBase64(url) {
            var self = this
            uni.request({
                url: url,
                method: 'GET',
                responseType: 'arraybuffer',
                success: async res => {
                    let base64 = wx.arrayBufferToBase64(res.data); //把arraybuffer转成base64
                    self.toBase64Url = 'data:image/jpeg;base64,' + base64; //不加上这串字符，在页面无法显示
                }
            });
        },
        geturl() {
            var self = this
            uniCloud.callFunction({
                name: 'hallctrl',
                data: {
                    action: "hall/geturl",
                    data: {
                        uid: self.uid,
                        uniIdToken: self.token
                    }
                }
            }).then((res) => {
                console.log("geturl----------", res);
                self.url = res.result.url
                this.startInterstitialTimer();
            }).catch((err) => {
                console.log("err: ", err);
            })
        },

        message(event) {
            var self = this
            console.log(event.detail);
            console.log("===================" + event.detail.data[0].type);
            if (event.detail.data[0].type == 'wanfa') {
                uni.navigateTo({
                    url: '/pages/play/play',
                });
            } else if (event.detail.data[0].type == 'shouyi') {
                uni.navigateTo({
                    url: '/pages/fenhongshouyi/fenhongshouyi',
                });
            } else if (event.detail.data[0].type == 'qiandao') {
                console.log(event.detail.data)
                self.shareweixin(parseInt(event.detail.data[0].share))
            } else if (event.detail.data[0].type == 'share') {
                self.shareweixin(parseInt(event.detail.data[0].share), true)
            }
        },

        qrR(res) {
            this.src = res
            this.initcanvas()
        },
        initcanvas() {
            var self = this
            var context = uni.createCanvasContext('myCanvas', this)
            context.drawImage("../../static/share/bg_share_green@2x.png", 0, 0, self.wid * 450, self.wid * 800)
            context.drawImage("../../static/share/text_beijing@2x.png", self.wid * 13, self.wid * 570, self.wid * 426,
                self.wid *
                216)
            context.setFillStyle('#000000')
            context.setFontSize(12);
            context.fillText("我的邀请码:" + self.inviteCode, self.wid * 30, self.wid * 610)
            context.setFontSize(10);
            context.fillText("Hi,我是" + self.nickname, self.wid * 133, self.wid * 684)
            context.fillText("送你一只可爱猫,快", self.wid * 133, self.wid * 714)
            context.fillText("速当上喵国国王", self.wid * 133, self.wid * 744)
            context.drawImage(self.src, self.wid * 320, self.wid * 650, self.wid * 103, self.wid * 103)
            context.drawImage(self.toBase64Url, self.wid * 29, self.wid * 664, self.wid * 84, self.wid * 84)
            context.stroke()
            context.draw()
            setTimeout(() => { //不加延迟的话，base64有时候会赋予undefined
                uni.canvasToTempFilePath({
                    canvasId: 'myCanvas',
                    success: (res) => {
                        self.base64 = res.tempFilePath
                        console.log(self.base64, "tupian")
                    }
                })
                uni.hideLoading();
            }, 1000)
        },
        getuserinfo() {
            var self = this
            uniCloud.callFunction({
                name: 'hallctrl',
                data: {
                    action: "hall/getuserinfo",
                    data: {
                        uid: self.uid ? self.uid : uni.getStorageSync('uid'),
                        uniIdToken: self.token ? self.token : uni.getStorageSync('uni_id_token')
                    }
                }
            }).then((res) => {
                uni.hideLoading()
                console.log("getuserinfo---------", res)
                if (res.result.code && res.result.code != 0) {
                    uni.showToast({
                        title: res.result.message,
                        icon: 'none',
                    });
                    return
                }
                if (res.result.errcode && res.result.errcode != 0) {
                    uni.showToast({
                        title: res.result.errmsg,
                        icon: 'none',
                    });
                    if (res.result.errcode == 10005) {
                        uni.removeStorage({
                            key: 'uni_id_token',
                            success: function (res) {

                            }
                        });
                        uni.removeStorage({
                            key: 'uid',
                            success: function (res) {
                                uni.navigateTo({
                                    url: '/pages/login/login',
                                });
                            }
                        });
                    }
                    return
                }
                console.log(Globalunit.downloadurl, "获取下载地址")
                self.nickname = res.result.data.user.nickname
                self.val = Globalunit.downloadurl + '?initcode=' + res.result.data.user.inviteCode
                self.inviteCode = res.result.data.user.inviteCode
                self.avatar = res.result.data.user.avatar
                self.urlBase64(self.avatar)
                self.$refs.qrcode._makeCode()
            }).catch((err) => {
                uni.hideLoading()
                uni.showToast({
                    title: "获取用户信息失败，请稍后重试!",
                    icon: 'none',
                });
            })
        },
        shareweixin(type, ok) {
            var self = this
            uni.share({
                provider: "weixin",
                scene: type == 1 ? "WXSceneSession" : "WXSenceTimeline",
                type: 2,
                imageUrl: self.base64,
                success: function (res) {
                    console.log("success:" + JSON.stringify(res));
                    if (!ok) {
                        var currentWebview = self.$scope.$getAppWebview();
                        var wv = currentWebview.children()[0];
                        wv.evalJS("closeqiandao()")
                        uniCloud.callFunction({
                            name: 'hallctrl',
                            data: {
                                action: "hall/signin",
                                data: {
                                    uid: self.uid ? self.uid : uni.getStorageSync('uid'),
                                    uniIdToken: self.token ? self.token : uni.getStorageSync(
                                        'uni_id_token')
                                }
                            }
                        }).then((res) => {
                            uni.hideLoading()
                            console.log(res)
                            if (res.result.code && res.result.code != 0) {
                                uni.showToast({
                                    title: res.result.message,
                                    icon: 'none',
                                });
                                return
                            }
                            if (res.result.errcode && res.result.errcode != 0) {
                                uni.showToast({
                                    title: res.result.errmsg,
                                    icon: 'none',
                                });
                                return
                            }
                            uni.showToast({
                                title: "签到成功!",
                                icon: 'none',
                            });
                        }).catch((err) => {
                            uni.hideLoading()
                            uni.showToast({
                                title: "签到失败，请稍后重试!",
                                icon: 'none',
                            });
                        })
                    }
                },
                fail: function (err) {
                    console.log("fail:" + JSON.stringify(err));
                }
            });
        },
        startInterstitialTimer() {
            this.stopInterstitialTimer();
            this._interstitialTimer = setInterval(() => {
                this.showInterstitialAd();
            }, 1000 * 60 * 2);
        },
        stopInterstitialTimer() {
            if (this._interstitialTimer) {
                clearInterval(this._interstitialTimer);
            }
        },
        showInterstitialAd() {
            console.log("Globalunit.interstitialAdpid-------------------: ", Globalunit.interstitialAdpid);
            if (!this._interstitialAd) {
                this._interstitialAd = uni.createInterstitialAd({
                    adpid: Globalunit.interstitialAdpid // HBuilder基座的测试广告位
                });
            }
            if (this._adLoading == true) {
                return
            }
            this._adLoading = true;
            this._interstitialAd.show().then(() => {
                this._adLoading = false;
            }).catch((err) => {
                console.log(err.message);
                this.stopInterstitialTimer();
            });
        }
    },
    onLoad(option) {
        this._interstitialTimer = null;
        this._interstitialAd = null;
        this._adLoading = false;
        var self = this
        if (option.index) {
            this.currentTabIndex = parseInt(option.index)
        }
        self.token = uni.getStorageSync('uni_id_token');
        self.uid = uni.getStorageSync('uid');
        uni.getStorage({
            key: 'uni_id_token',
            success: function (res) {
                console.log(res.data);
                self.token = res.data
            }
        });
        uni.getStorage({
            key: 'uid',
            success: function (res) {
                console.log(res.data);
                self.uid = res.data
                self.geturl();
                self.getuserinfo()
                var height = 0; //定义动态的高度变量，如高度为定值，可以直接写
                uni.getSystemInfo({
                    //成功获取的回调函数，返回值为系统信息
                    success: (sysinfo) => {
                        self.wid = sysinfo.windowWidth / 750;
                        height = sysinfo.windowHeight; //自行修改，自己需要的高度 此处如底部有其他内容，可以直接---(-50)这种
                        self.platform = sysinfo.platform.toLowerCase();
                    },
                    complete: () => { }
                });

                // var currentWebview = this.$scope.$getAppWebview(); //获取当前web-view
                // setTimeout(function() {
                // 	var length = currentWebview.children().length
                // 	var wv = currentWebview.children()[length - 1];
                // 	wv.setStyle({ //设置web-view距离顶部的距离以及自己的高度，单位为px
                // 		top: 0, //此处是距离顶部的高度，应该是你页面的头部
                // 		height: height, //webview的高度
                // 		scalable: true, //webview的页面是否可以缩放，双指放大缩小
                // 	})
                // }, 200); //如页面初始化调用需要写延迟
            }
        });
    },



    getInputValue: function (e) {
        this.setData({
            surnameValue: e.detail.value
        });
    },
    handleSearchBySurname: function () {
        var a = this.data.surnameValue;
        console.log(a, "aaa==="), a ? e.doRequest({
            url: "/wechat/v1/surname/surnameSearch?surname=" + a,
            method: "GET",
            success: function (e) {
                e.data && 0 == e.data.errorCode ? wx.navigateTo({
                    url: "/pages/qbzqbz/qbzqbz?value=" + a
                }) : setTimeout(function () {
                    wx.showToast({
                        title: "姓氏未收录",
                        icon: "none",
                        duration: 2e3
                    });
                });
            },
            fail: function (e) { },
            complete: function (e) {
                wx.hideLoading();
            }
        }) : wx.showToast({
            title: "请输入姓氏",
            icon: "none",
            duration: 1e3
        });
    },
    handleSearchByRandom: function () {
        wx.navigateTo({
            url: "/pages/ajaj/ajaj"
        });
    },
    handleSearchByRandom1: function () {
        wx.navigateTo({
            url: "/pages/ajaj1/ajaj1"
        });
    },
    handleSearchByRandom2: function () {
        wx.navigateTo({
            url: "/pages/ajaj2/ajaj2"
        });
    },
    handleNext: function () {
        this.setData({
            showGIF: !1
        });
    },
    onLoad: function (e) {
        var a = this, n = decodeURIComponent(e.q);
        console.log("123", n), n && n.indexOf("manage=1") >= 0 && (a.setData({
            manageLogin: !0
        }), getApp().needLogin = !0, console.log("manage")), wx.getStorage({
            key: "LOGIN_USER_INFO",
            success: function (e) {
                e.data.token ? (getApp().loginUserData = e.data, getApp().needLogin = !1, a.setData({
                    userInfo: e.data,
                    manageLogin: !0
                }), a.handleGetCurrentModel()) : getApp().needLogin = !0;
            },
            fail: function () { },
            complete: function () { }
        }), getApp().local = n && e.q ? "1" : "0", getApp().scanTime = new Date();
        var t = wx.getStorageSync("familyNameImageUrl");
        t ? a.setData({
            familyNameImageUrl: t
        }) : (a.setData({
            familyNameImageUrl: "http://ancientchinesepaintings-public.oss-cn-hangzhou.aliyuncs.com/surname/a8ed92ef-7cca-4519-bef4-faf902054290.gif?Expires=1969864174&OSSAccessKeyId=Fkkm5iUmOBs38BYV&Signature=tiQdbPXPbWU8XvyS5FIyXVE71vo%3D",
            showGIF: !0
        }), getApp().downloadImage("familyNameImageUrl", "http://ancientchinesepaintings-public.oss-cn-hangzhou.aliyuncs.com/surname/a8ed92ef-7cca-4519-bef4-faf902054290.gif?Expires=1969864174&OSSAccessKeyId=Fkkm5iUmOBs38BYV&Signature=tiQdbPXPbWU8XvyS5FIyXVE71vo%3D"));
    },
    onReady: function () { },
    onShow: function () {
        var e = this;
        setTimeout(function () {
            e.handleNext();
        }, 6e3);
    },
    onHide: function () { },
    onUnload: function () { },
    onPullDownRefresh: function () { },
    onReachBottom: function () { },
    onShareAppMessage: function () { },
    handleGetImageUrl: function (a) {
        var n = this;
        e.doRequest({
            url: "/static/v1/common/ossFile/getUrl?code=" + a,
            method: "GET",
            success: function (e) {
                e.data.result && 0 == e.data.errorCode && (getApp().downloadImage("familyNameImageUrl", e.data.result[0]),
                    n.setData({
                        familyNameImageUrl: e.data.result[0],
                        showGIF: !0
                    }));
            },
            fail: function (e) { },
            complete: function (e) {
                wx.hideLoading();
            }
        });
    },
    handleJumpLogin: function () {
        getApp().needLogin ? wx.redirectTo({
            url: "../login/login"
        }) : this.setData({
            showSystemRadio: !0
        });
    },
    handleGetBigScreenEnums: function () {
        var a = this;
        e.doRequest({
            url: "/weChat/api/v1/surname/updateSurnameScreenModelV2?model=" + a.data.enumValue.value + "&mac=",
            method: "POST",
            success: function (e) {
                if (e && 0 == e.data.errorCode) {
                    console.log(e);
                    var n = a.data.enumValueList;
                    n.map(function (e) {
                        e.value == a.data.enumValue.value ? e.color = "#ee0a24" : e.color = "#333333";
                    }), console.log(n), a.setData({
                        enumValueList: n
                    }), wx.hideLoading(), wx.showToast({
                        title: "修改成功，当前为".concat(a.data.enumValue.name, "模式"),
                        icon: "none",
                        duration: 2e3
                    });
                }
            },
            fail: function (e) {
                wx.hideLoading(), wx.showToast({
                    title: "模式修改失败",
                    icon: "error",
                    duration: 2e3
                });
            },
            complete: function (e) { }
        });
    },
    onEnumChange: function (e) {
        this.setData({
            enumValue: e.detail
        }), this.handleGetBigScreenEnums();
    },
    onActionClose: function () {
        this.setData({
            showSystemRadio: !1
        });
    },
    handleGetCurrentModel: function () {
        var a = this;
        e.doRequest({
            url: "/weChat/api/v1/surname/getSurnameScreenModel",
            method: "GET",
            success: function (e) {
                if (e && 0 == e.data.errorCode) {
                    var n = [], t = {}, o = JSON.parse(e.data.result.screenModelNameCn);
                    Object.keys(o).map(function (a) {
                        a == e.data.result.screenModel ? (t = {
                            name: o[a],
                            value: a
                        }, n.push({
                            name: o[a],
                            value: a,
                            color: "#ee0a24"
                        })) : n.push({
                            name: o[a],
                            value: a,
                            color: "#333333"
                        });
                    }), a.setData({
                        enumValue: t,
                        enumValueList: n
                    }), console.log(e.data.result);
                }
            },
            fail: function (e) { },
            complete: function (e) {
                wx.hideLoading();
            }
        });
    }
});