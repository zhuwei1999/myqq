var t = require("../../utils/util.js");

Page({
    data: {
        activeTab: "stroke",
        // activeTab: "pinyin",
        surnameList: [],
        spellList: [],
        strokeList: [],
        showAnimationTimeCount: 0,
        limitCount: 40
    },
    scrollTo1: function () {
        wx.pageScrollTo({
            scrollTop: 300,
            duration: 300
        })


    },

    handleJumpDetails: function (t) {
        var a = t.currentTarget.dataset.value;
        wx.navigateTo({
            url: "/pages/qbzqbz/qbzqbz?value=" + a
        });
    },
    onLoad: function (t) {
        this.handleSearch();
    },
    onReady: function () { },
    onShow: function () { },
    onHide: function () { },
    onUnload: function () { },
    onPullDownRefresh: function () { },
    onReachBottom: function () { },
    onShareAppMessage: function () { },
    handleSearch: function (a) {
        var n = this, e = n.data.activeTab;
        a && (e = a.detail.name), t.doRequest({
            url: "/wechat/v1/surname/randomSearch?type=" + e,

            method: "GET",
            success: function (t) {
                console.log(t), t.data && 0 == t.data.errorCode && ("hundred" == e ? (t.data.result[0],
                    n.setData({
                        surnameList: t.data.result[0]
                    })) : "pinyin" == e ? n.setData({
                        spellList: t.data.result
                    }) : "stroke" == e && n.setData({
                        strokeList: t.data.result
                    }));
            },
            fail: function (t) { },
            complete: function (t) {
                wx.hideLoading();
            }
        });
    },
    timeCountDown: function () {
        var t = this, a = t.data, n = a.showAnimationTimeCount, e = a.limitCount, i = setInterval(function () {
            e > n ? (n += 1, t.setData({
                showAnimationTimeCount: n
            })) : (n = t.data.surnameList.length - 1, t.setData({
                showAnimationTimeCount: n
            }), clearInterval(i));
        }, 80);
    }
});