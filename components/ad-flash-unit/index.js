var t = require("../../@babel/runtime/helpers/regeneratorRuntime"), e = require("../../@babel/runtime/helpers/asyncToGenerator"), n = null, a = getApp(), o = new Date(), r = "".concat(o.getFullYear(), "-").concat(o.getMonth() + 1, "-").concat(o.getDate());

Component({
    properties: {
        ad_type: {
            type: String
        }
    },
    data: {
        count: 0,
        isShow: !1,
        config: {}
    },
    methods: {
        positionClick: function() {},
        forceClick: function() {},
        setShowAd: function() {
            wx.setStorageSync("FiXED_AD_DATE", r);
        },
        checkIsShowAd: function() {
            return wx.getStorageSync("FiXED_AD_DATE") === r;
        },
        requset: function() {
            var t = this;
            return new Promise(function(e, n) {
                var a = wx.getAccountInfoSync(), o = getCurrentPages(), r = o[o.length - 1].route;
                wx.request({
                    url: "".concat("https://mpss.meitry.com", "/api/get_ad_config"),
                    method: "POST",
                    data: {
                        appid: a.miniProgram.appId,
                        customId: "",
                        path: r,
                        adType: t.data.ad_type
                    },
                    success: function(t) {
                        e(t.data);
                    }
                });
            });
        },
        randomTime: function(t, e) {
            return Math.ceil(Math.random() * (e - t + 1) + t - 1);
        },
        sleep: function(t) {
            return new Promise(function(e) {
                return setTimeout(e, t);
            });
        },
        flashRate: function() {
            var t = this;
            this.setData({
                isShow: !0
            }), n = setTimeout(function() {
                t.setData({
                    isShow: !1
                }), a.globalData.adCount > 0 && !t.checkIsShowAd() && t.flashRate();
            }, 1e3 * t.data.config.show_time);
        }
    },
    lifetimes: {
        attached: function() {
            var a = this;
            return e(t().mark(function e() {
                var o, r, i, c, s;
                return t().wrap(function(t) {
                    for (;;) switch (t.prev = t.next) {
                      case 0:
                        return o = a, r = a.data.config, i = r.flash_freq_end, c = r.flash_freq_start, t.next = 4, 
                        o.requset();

                      case 4:
                        s = t.sent, s.day_click, o.setData({
                            config: s
                        }), o.setData({
                            count: s.day_click
                        }), n = setTimeout(function() {
                            o.flashRate();
                        }, 1e3 * o.randomTime(i, c));

                      case 9:
                      case "end":
                        return t.stop();
                    }
                }, e);
            }))();
        },
        detached: function() {
            clearTimeout(n);
        }
    },
    options: {
        styleIsolation: "isolated",
        multipleSlots: !0
    }
});