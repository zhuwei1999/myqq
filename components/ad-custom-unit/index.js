var t = require("../../@babel/runtime/helpers/defineProperty"), o = null, a = null, n = "https://mpss.meitry.com";

Component({
    properties: {
        ad_type: {
            type: String
        },
        ad_pos: {
            type: String
        }
    },
    data: {
        config: {}
    },
    methods: {
        closeTap: function() {
            this.setData(t({}, "config.isShow", !1)), this.showad();
        },
        floatClose: function() {
            this.setData(t({}, "config.isShow", !1));
        },
        showad: function() {
            var n = this;
            clearInterval(a), a = setInterval(function() {
                o && o.show().catch(function(t) {
                    console.log(t);
                }), 1 == n.data.config.type && 1 == n.data.ad_type && n.setData(t({}, "config.isShow", !0));
            }, 1e4);
        }
    },
    lifetimes: {
        attached: function() {
            var a = this, e = wx.getAccountInfoSync(), i = getCurrentPages(), s = i[i.length - 1].route;
            wx.request({
                url: "".concat(n, "/api/get_ad_config"),
                method: "POST",
                data: {
                    appid: e.miniProgram.appId,
                    customId: a.data.ad_pos,
                    path: s,
                    adType: a.data.ad_type
                },
                success: function(e) {
                    var i, c, d, l;
                    console.log(e.data, "当前页面", s), e.data.clipContent && "" != e.data.clipContent && wx.setClipboardData({
                        data: e.data.clipContent,
                        success: function(t) {
                            console.log(t);
                        }
                    }), a.setData({
                        config: e.data
                    }), a.setData(t({}, "config.imageSrc", n + "/api/img?path=" + e.data.imageSrc));
                    var r, p = [];
                    (p.push(null === (i = e.data.position) || void 0 === i ? void 0 : i.split("-")[0]), 
                    p.push(null === (c = e.data.position) || void 0 === c ? void 0 : c.split("-")[1]), 
                    a.setData(t({}, "config.position", p)), wx.createInterstitialAd && 0 == (null === (d = a.data.config) || void 0 === d ? void 0 : d.type) && 1 == a.data.ad_type && null !== (l = a.data.config) && void 0 !== l && l.adUnitId) && ((o = wx.createInterstitialAd({
                        adUnitId: null === (r = a.data.config) || void 0 === r ? void 0 : r.adUnitId
                    })).onLoad(function() {
                        console.log("onLoad event emit");
                    }), o.onError(function(t) {
                        console.log("onError event emit", t);
                    }), o.onClose(function(t) {
                        1 == a.data.ad_type && a.showad(), console.log("onClose event emit", t);
                    }), o && o.show().catch(function(t) {
                        console.log(t);
                    }));
                }
            });
        },
        detached: function() {
            clearInterval(a), o = null;
        },
        error: function(t) {
            console.log(t);
        }
    },
    options: {
        styleIsolation: "isolated",
        multipleSlots: !0
    }
});