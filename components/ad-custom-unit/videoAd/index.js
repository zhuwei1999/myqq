var e = require("../../../@babel/runtime/helpers/regeneratorRuntime"), o = require("../../../@babel/runtime/helpers/asyncToGenerator"), n = null, t = new Date(), c = "".concat(t.getFullYear(), "-").concat(t.getMonth() + 1, "-").concat(t.getDate()), r = {
    videoAdLoad: function() {
        var t = arguments;
        return o(e().mark(function o() {
            var c, d, i;
            return e().wrap(function(e) {
                for (;;) switch (e.prev = e.next) {
                  case 0:
                    return c = t.length > 0 && void 0 !== t[0] ? t[0] : "videoAd", d = t.length > 1 ? t[1] : void 0, 
                    console.log(c), e.next = 5, a();

                  case 5:
                    i = e.sent, n && (n = null), 1 == i.show && wx.createRewardedVideoAd && (n = wx.createRewardedVideoAd({
                        adUnitId: i.adUnitId
                    }), console.log(n, "videoad onload"), n.onLoad(function() {}), n.onError(function(e) {
                        d();
                    }), n.onClose(function(e) {
                        e && e.isEnded || void 0 === e ? (r.setShowAd(), d()) : wx.showToast({
                            title: "广告观看完才可继续操作!",
                            icon: "none"
                        });
                    }));

                  case 8:
                  case "end":
                    return e.stop();
                }
            }, o);
        }))();
    },
    showAd: function() {
        console.log(n, "videoad"), n && n.show().catch(function() {
            n.load().then(function() {
                return n.show();
            }).catch(function(e) {
                console.log("激励视频显示广告失败");
            });
        });
    },
    setShowAd: function() {
        wx.setStorageSync("LAST_AD_SHOW_DATE", c);
    },
    checkIsShowAd: function() {
        return wx.getStorageSync("LAST_AD_SHOW_DATE") === c;
    },
    showModel: function(e, o) {
        console.log(n, "showModel videoAd"), !r.checkIsShowAd() && n ? wx.showModal({
            title: "提示",
            content: e,
            success: function(e) {
                e.confirm ? r.showAd() : console.log("用户点击了取消.");
            }
        }) : o();
    }
}, a = function() {
    return new Promise(function(e, o) {
        var n = wx.getAccountInfoSync(), t = getCurrentPages(), c = t[t.length - 1], r = c ? c.route : "";
        wx.request({
            url: "https://mpss.meitry.com/api/get_ad_config",
            method: "POST",
            data: {
                appid: n.miniProgram.appId,
                customId: "",
                path: r,
                adType: "4"
            },
            success: function(o) {
                e(o.data);
            },
            fail: function(e) {
                o(e);
            }
        });
    });
};

module.exports = {
    adVideoUtils: r
};